package it.unisalento.pas.smartcitywastemanagement.restcontrollers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import it.unisalento.pas.smartcitywastemanagement.domain.WasteContainer;
import it.unisalento.pas.smartcitywastemanagement.dto.WasteContainerDTO;
import it.unisalento.pas.smartcitywastemanagement.repositories.WasteContainerRepository;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/api/waste-containers")
public class WasteContainerRestController {

    @Autowired
    WasteContainerRepository wasteContainerRepository;

    @PreAuthorize("hasAnyRole('AGENCY', 'ADMIN','CITIZEN')")

    @GetMapping("/")
    public List<WasteContainerDTO> getAllWasteContainers() {
        List<WasteContainerDTO> dtoList = new ArrayList<>();
        List<WasteContainer> wasteContainers = wasteContainerRepository.findAll();
        for (WasteContainer container : wasteContainers) {
            WasteContainerDTO dto = new WasteContainerDTO();

            dto.setId(container.getId());
            dto.setType(container.getType());
            dto.setRefWasteContainerModel(container.getRefWasteContainerModel());
            dto.setSerialNumber(container.getSerialNumber());
            dto.setLatitude(container.getLatitude());
            dto.setLongitude(container.getLongitude());
            dto.setVia(container.getVia());

            dtoList.add(dto);
        }
        return dtoList;
    }
    @PreAuthorize("hasAnyRole('AGENCY', 'ADMIN')")

    @GetMapping("/{id}")
    public WasteContainerDTO getWasteContainerById(@PathVariable String id) {
        WasteContainer wasteContainer = wasteContainerRepository.findById(id).orElse(null);
        if (wasteContainer == null) {
            return null;
        }
        WasteContainerDTO dto = new WasteContainerDTO();

        dto.setId(wasteContainer.getId());
        dto.setType(wasteContainer.getType());
        dto.setRefWasteContainerModel(wasteContainer.getRefWasteContainerModel());
        dto.setSerialNumber(wasteContainer.getSerialNumber());
        dto.setLatitude(wasteContainer.getLatitude());
        dto.setLongitude(wasteContainer.getLongitude());
        dto.setVia(wasteContainer.getVia());
        return dto;
    }

    @PreAuthorize("hasAnyRole('AGENCY', 'ADMIN')")
    @PostMapping("/")
    public WasteContainerDTO createWasteContainer(@RequestBody WasteContainerDTO wasteContainerDTO) {
        List<WasteContainer> containers = wasteContainerRepository.findTopByOrderByIdDesc(PageRequest.of(0, 1));
        WasteContainer lastContainer = containers.isEmpty() ? null : containers.get(0);

        String newId = "wastecontainer:Lecce:001"; // Default ID if no containers exist
        if (lastContainer != null && !StringUtils.isEmpty(lastContainer.getId())) {
            String lastId = lastContainer.getId();
            System.out.println("Ultimo cassonetto inserito con ID: " + lastContainer.getId());

            String idNumberStr = lastId.substring(lastId.lastIndexOf(":") + 1);
            try {
                int idNumber = Integer.parseInt(idNumberStr);
                newId = String.format("wastecontainer:Lecce:%03d", idNumber + 1);
            } catch (NumberFormatException e) {
                // Handle the exception if the ID is not in the expected format
                throw new RuntimeException("Error parsing ID number from the last container");
            }
        }

        WasteContainer wasteContainer = new WasteContainer();
        wasteContainer.setId(newId);
        wasteContainer.setType(wasteContainerDTO.getType());
        wasteContainer.setRefWasteContainerModel(wasteContainerDTO.getRefWasteContainerModel());
        wasteContainer.setSerialNumber(wasteContainerDTO.getSerialNumber());
        wasteContainer.setLatitude(wasteContainerDTO.getLatitude());
        wasteContainer.setLongitude(wasteContainerDTO.getLongitude());
        wasteContainer.setVia(wasteContainerDTO.getVia());

        wasteContainer = wasteContainerRepository.save(wasteContainer);
        wasteContainerDTO.setId(wasteContainer.getId());
        return wasteContainerDTO;
    }

    @PreAuthorize("hasAnyRole('AGENCY', 'ADMIN')")
    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteWasteContainerById(@PathVariable String id) {
        if (!wasteContainerRepository.existsById(id)) {
            return new ResponseEntity<>("Waste container not found with ID: " + id, HttpStatus.NOT_FOUND);
        }

        wasteContainerRepository.deleteById(id);
        return new ResponseEntity<>("Waste container deleted successfully with ID: " + id, HttpStatus.OK);
    }

    @PreAuthorize("hasAnyRole('AGENCY', 'ADMIN', 'EMPLOYEE','SCHEDULING','COMUNE')")
    @PostMapping("/batch")
    public ResponseEntity<List<WasteContainerDTO>> getWasteContainersByIds(@RequestBody List<String> ids) {
        List<WasteContainerDTO> dtoList = new ArrayList<>();

        if (ids == null || ids.isEmpty()) {
            return ResponseEntity.badRequest().body(dtoList); // Restituisce una risposta vuota o un errore se l'input è vuoto
        }

        for (String id : ids) {
            wasteContainerRepository.findById(id).ifPresent(wasteContainer -> {
                WasteContainerDTO dto = new WasteContainerDTO();
                dto.setId(wasteContainer.getId());
                dto.setType(wasteContainer.getType()); // Assicurati che questo campo esista nel tuo DTO e nel tuo dominio
                dto.setRefWasteContainerModel(wasteContainer.getRefWasteContainerModel()); // Assicurati che questo campo esista
                dto.setSerialNumber(wasteContainer.getSerialNumber()); // Assicurati che questo campo esista
                dto.setLatitude(wasteContainer.getLatitude());
                dto.setLongitude(wasteContainer.getLongitude());
                dto.setVia(wasteContainer.getVia());
                dtoList.add(dto);
            });
        }

        return ResponseEntity.ok(dtoList);
    }

    @PreAuthorize("hasAnyRole('AGENCY', 'ADMIN')")
    @PostMapping("/sort")
    public ResponseEntity<List<String>> sortWasteContainers(@RequestBody List<String> containerIds) {
        List<WasteContainerDTO> containers = new ArrayList<>();

        for (String id : containerIds) {
            wasteContainerRepository.findById(id).ifPresent(wasteContainer -> {
                WasteContainerDTO dto = new WasteContainerDTO();
                dto.setId(wasteContainer.getId());
                dto.setLatitude(wasteContainer.getLatitude());
                dto.setLongitude(wasteContainer.getLongitude());
                containers.add(dto);
            });
        }

        if (containers.isEmpty()) {
            return ResponseEntity.noContent().build();
        }

        List<WasteContainerDTO> sortedContainers = sortContainersByNearestNeighbour(containers);
        List<String> sortedIds = new ArrayList<>();

        for (WasteContainerDTO container : sortedContainers) {
            sortedIds.add(container.getId());
        }

        return ResponseEntity.ok(sortedIds);
    }

    private List<WasteContainerDTO> sortContainersByNearestNeighbour(List<WasteContainerDTO> containers) {
        List<WasteContainerDTO> sorted = new ArrayList<>();
        WasteContainerDTO current = containers.get(0);
        sorted.add(current);
        containers.remove(0);

        while (!containers.isEmpty()) {
            final WasteContainerDTO finalCurrent = current;
            containers.sort(Comparator.comparing(c -> distance(finalCurrent, c)));
            current = containers.get(0);
            sorted.add(current);
            containers.remove(0);
        }

        return sorted;
    }

    private double distance(WasteContainerDTO c1, WasteContainerDTO c2) {
        double latDiff = c1.getLatitude() - c2.getLatitude();
        double lngDiff = c1.getLongitude() - c2.getLongitude();
        return Math.sqrt(latDiff * latDiff + lngDiff * lngDiff);
    }

    @PreAuthorize("hasAnyRole('AGENCY', 'ADMIN')")
    @PutMapping("/update")
    public ResponseEntity<?> updateWasteContainerLocation(@RequestBody WasteContainerDTO wasteContainerDTO) {
        WasteContainer wasteContainer = wasteContainerRepository.findById(wasteContainerDTO.getId()).orElse(null);
        if (wasteContainer == null) {
            return new ResponseEntity<>("Waste container not found with ID: " + wasteContainerDTO.getId(), HttpStatus.NOT_FOUND);
        }

        wasteContainer.setLatitude(wasteContainerDTO.getLatitude());
        wasteContainer.setLongitude(wasteContainerDTO.getLongitude());
        wasteContainer.setVia(wasteContainerDTO.getVia());

        wasteContainerRepository.save(wasteContainer);
        return new ResponseEntity<>("Waste container updated successfully with ID: " + wasteContainer.getId(), HttpStatus.OK);
    }


}

