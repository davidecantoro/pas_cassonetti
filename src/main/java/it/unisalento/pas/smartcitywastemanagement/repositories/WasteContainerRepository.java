package it.unisalento.pas.smartcitywastemanagement.repositories;
import org.springframework.data.domain.Pageable;

import it.unisalento.pas.smartcitywastemanagement.domain.WasteContainer;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;


import java.util.List;

public interface WasteContainerRepository extends MongoRepository<WasteContainer, String> {

    @Query(value = "{}", sort = "{ 'id' : -1 }")
    List<WasteContainer> findTopByOrderByIdDesc(Pageable pageable);
}